pilvo-code-test

test can be run form intellij/eclipce testNg plugin or using gradle tasks
gradle tasks 
1. "gradlew apiTest" - to run api tests
2. "gradlew uiTest" - to run ui test


Note:
-gradle is used for dependancy managemnet and it should be able to download all the dependancies.
-If proxy is place then need to pass necessary information in BaseTest of api tests
Line 28 : RestAssured.proxy = host("0.0.0.0").withPort(1111).withAuth("aaaa","aaaa"); //un-comment this line if using proxy
-Tests html reports will be generated in "Build" folder of the project for both gradle run or the testNG plugin run
-UI tests reports can be improved but I dint get much time to work on that so basic console output are written as of now