package com.pilvo.api.base;

import io.restassured.RestAssured;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static io.restassured.RestAssured.basic;
import static io.restassured.specification.ProxySpecification.host;

public class BaseTest {

    protected Properties properties;
    private String PROP_FILE = "project.properties";

    public BaseTest() {
        System.out.println("####################################################################");
        System.out.println("#####          TESTS FOR MESSAGE SERVICE                        ####");
        System.out.println("####################################################################");

        //read properties
        properties = loadProperties(PROP_FILE);

        RestAssured.useRelaxedHTTPSValidation();
        //RestAssured.proxy = host("0.0.0.0").withPort(1111).withAuth("aaaa","aaaa"); //un-comment this line if using proxy
        RestAssured.authentication = basic(properties.getProperty("auth-id"),properties.getProperty("auth-token"));
    }


    public static Properties loadProperties(String fileName) {
        try {
            String filePath =
                    System.getProperty("user.dir")
                            + File.separator + "src" + File.separator + "test" + File.separator + "resources"
                            + File.separator + fileName;

            Properties properties = new Properties();
            InputStream input = new FileInputStream(new File(filePath));
            properties.load(input);

            System.out.println("Loaded properties - "+ fileName);
            System.out.println("properties : "+properties);

            return properties;
        } catch (IOException e) {
            System.out.println("Failed to find properties file");
            throw new RuntimeException(e);
        }
    }


}
