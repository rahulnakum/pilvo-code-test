package com.pilvo.api.services;

import com.pilvo.api.base.BaseTest;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

import static io.restassured.RestAssured.expect;


public class MessageApiTest extends BaseTest{

    private List<String> listNumbers = null;
    private String message_uuid;

    @Test(priority = 1)
    public void validateTwoNumbers(){
        Response responseGetNumbers = expect()
                .given()
                .pathParam("auth-id",properties.getProperty("auth-id"))
                .get("https://api.plivo.com/v1/Account/{auth-id}/Number/?limit=2")
                .then().extract().response();

        System.out.println("Get Numbers service Response : " + responseGetNumbers.asString().replaceAll("\n",""));

        Assert.assertEquals(responseGetNumbers.statusCode(),200,"HTTP status code did not match");
        listNumbers = responseGetNumbers.jsonPath().getList("objects.number");//get numbers
        System.out.println("numbers returned are : "+listNumbers);
        //validate if two numbers are returned. We have added query parameter limit=2 so two numbers should be returned
        Assert.assertEquals(listNumbers.size(),2,"Get Numbers service did not return two numbers");
    }

    @Test(priority = 2,dependsOnMethods = "validateTwoNumbers")
    public void validateSendMessage(){
        String src = listNumbers.get(0);
        String dest = listNumbers.get(1);
        String message = "testMessage123451234!@@!#$";
        String requestBody = "{\n" +
                "\"src\":\""+src+"\",\n" +
                "\"dst\":\""+dest+"\",\n" +
                "\"text\":\""+message+"\"\n" +
                "}";
        String expectedMessage = "message(s) queued";

        Response responseSendMessage =  expect()
                .given()
                .contentType("application/json")
                .pathParam("auth-id",properties.getProperty("auth-id"))
                .body(requestBody)
                .post(properties.getProperty("sendMessageUrl"))
                .then().extract().response();

        System.out.println("Send Message service Response : " + responseSendMessage.asString().replaceAll("\n",""));

        Assert.assertEquals(responseSendMessage.statusCode(),202,"HTTP status code did not match");
        List<String>listMessageUUIDs = responseSendMessage.jsonPath().getList("message_uuid");
        Assert.assertTrue(listMessageUUIDs.size()>0,"message uuid is not present");
        message_uuid = listMessageUUIDs.get(0);
        System.out.println("message_uuid : "+message_uuid);
        Assert.assertEquals(responseSendMessage.jsonPath().getString("message"),expectedMessage,"response message did not match");
    }

    @Test(priority = 3,dependsOnMethods = "validateSendMessage")
    public void validategetMessageDetail(){
        String messageDetailTotalRate;
        String pricingDetailOutBoundRate;
        String country_iso="US";
        Response responseGetMessageDetails = expect()
                .given()
                .pathParam("auth-id",properties.getProperty("auth-id"))
                .pathParam("message_uuid",message_uuid)
                .get(properties.getProperty("getMessageDetailsUrl"))
                .then().extract().response();

        System.out.println("Get Message Detail service Response : " + responseGetMessageDetails.asString().replaceAll("\n",""));
        Assert.assertEquals(responseGetMessageDetails.statusCode(),200,"Message Detail HTTP status code did not match");
        messageDetailTotalRate = responseGetMessageDetails.jsonPath().getString("total_rate");
        System.out.println("Get Message Detail total_rate : "+messageDetailTotalRate);


        //Get Pricing Detail
        Response responseGetPricingDetails = expect()
                .given()
                .pathParam("auth-id",properties.getProperty("auth-id"))
                .queryParam("country_iso",country_iso)
                .get(properties.getProperty("getPricingForCountryUrl"))
                .then().extract().response();

        System.out.println("Get Pricing Detail service Response : " + responseGetPricingDetails.asString().replaceAll("\n",""));
        Assert.assertEquals(responseGetPricingDetails.statusCode(),200,"Pricing Detail HTTP status code did not match");
        pricingDetailOutBoundRate = responseGetPricingDetails.jsonPath().getString("message.outbound.rate");
        System.out.println("pricing Detail Outbound Rate : " + pricingDetailOutBoundRate);

        Assert.assertEquals(messageDetailTotalRate,pricingDetailOutBoundRate,"Rates did not match");
    }

}
