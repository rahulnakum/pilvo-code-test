package com.pilvo.ui;

import com.pilvo.ui.base.BaseTest;
import com.pilvo.ui.pages.CreateAppPage;
import com.pilvo.ui.pages.HomePage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

public class QuickFocusAppTests extends BaseTest {

    @BeforeClass
    public void setupTest() {
        WebDriverManager.chromedriver().setup();  //downloads chrome driver and set path automatically
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        homepage = new HomePage(driver);
        createApppage = new CreateAppPage(driver);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @Test
    public void test() throws InterruptedException {
        String pageName = "testPage";
        driver.get(properties.getProperty("baseUrl"));
        homepage.clickCreateApp();
        createApppage.clickLetsStarted();
        createApppage.createPage(pageName);
        createApppage.addSteps();
    }

    @AfterClass
    public void teardown() {
        if (driver != null) {
            //driver.quit();
        }
    }
}
