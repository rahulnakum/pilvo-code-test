package com.pilvo.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {
    WebDriver wd;

    private By btnCreateApp = By.id("link-create");

    public HomePage(WebDriver wd){
        this.wd=wd;
    }

    public void clickCreateApp(){
        wd.findElement(btnCreateApp).click();
        System.out.println("create page clicked");
    }
}
