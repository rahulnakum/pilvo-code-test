package com.pilvo.ui.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Set;

public class CreateAppPage {
    WebDriver wd;
    private By btnLetsGetStarted = By.xpath("//button[contains(text(),'started')]");

    private By btnAddnewPage = By.id("add-page");

    private By inputPageName = By.xpath("//p/input[@class='indented submitonenter']");
    private By btnCreatePage = By.xpath("//button[text()='Create' and @title='']");

    private By linkMessaging = By.partialLinkText("Messaging");
    private By btnSendMessage = By.xpath("//li[text()='Send an SMS']/a");
    private By sendMessageBox = By.xpath("//div[@id='module-1']");
    private By btnSendEmail = By.xpath("//li[text()='Send an Email']/a");
    private By sendemailBox = By.xpath("//div[@id='module-2']");

    private By roundStartNode = By.xpath("//div[@id='module-1']//div[contains(@id,'node')]");
    private By triangleSendMessage = By.xpath("//div[@id='module-2']/div/div[contains(@id,'rec-')]");
    private By triangleSendEmail = By.xpath("//div[@id='module-3']/div/div[contains(@id,'rec-')]");

    private By linkBasic = By.partialLinkText("Basic");
    private By btnHangUpOrExit = By.xpath("//li[text()='Hang Up or Exit']/a");
    private By hangUpOrExitBox = By.xpath("//div[@id='module-3']//div[2]");
    private By triangleExit = By.xpath("//div[@id='module-4']/div/div[contains(@id,'rec-')]");

    private By roundSMSSent = By.xpath("//div[@id='module-2']//div[@class='syn-node syn-node-attached-w ui-draggable syn-node-active']");
    private By roundSMSNotSent = By.xpath("//div[@id='module-2']//div[@class='syn-node syn-node-attached-e ui-draggable syn-node-active']");
    private By roundEmailSent = By.xpath("//div[@id='module-3']//div[@class='syn-node syn-node-attached-w ui-draggable syn-node-active']");
    private By roundEmailNotSent = By.xpath("//div[@id='module-3']//div[@class='syn-node syn-node-attached-e ui-draggable syn-node-active']");

    public CreateAppPage(WebDriver wd){
        this.wd=wd;
    }
    public void clickLetsStarted(){
        wd.findElement(btnLetsGetStarted).click();
    }
    public void createPage(String pageName) throws InterruptedException {
        wd.findElement(btnAddnewPage).click();
        List<WebElement> iframes = wd.findElements(By.tagName("iframe"));
        WebElement a = wd.switchTo().activeElement();
        wd.findElement(inputPageName).sendKeys(pageName);
        wd.findElement(btnCreatePage).click();
        System.out.println("page created successfully");
    }

    public void addSteps() throws InterruptedException {
        reliableClick(linkMessaging);
        reliableClick(btnSendMessage);
        new Actions(wd).dragAndDropBy(wd.findElement(sendMessageBox), -300, -300).build() .perform();
        System.out.println("send message step created");
        Thread.sleep(2000);
        reliableClick(btnSendEmail);
        new Actions(wd).dragAndDropBy(wd.findElement(sendemailBox), 200, -100).build() .perform();
        System.out.println("send email step created");
        Thread.sleep(2000);
        reliableClick(linkBasic);
        reliableClick(btnHangUpOrExit);
        new Actions(wd).dragAndDropBy(wd.findElement(hangUpOrExitBox), -200, 300).build() .perform();
        System.out.println("exit step created");
        Thread.sleep(2000);

        Actions act=new Actions(wd);
        act.dragAndDrop(wd.findElement(roundStartNode), wd.findElement(triangleSendMessage)).build().perform();

        Thread.sleep(3000);
        act.dragAndDrop(wd.findElement(roundSMSSent), wd.findElement(triangleExit)).build().perform();
        System.out.println("relationship created for send message 'sent'");
        Thread.sleep(3000);
        act.dragAndDrop(wd.findElement(roundSMSNotSent), wd.findElement(triangleSendEmail)).build().perform();
        System.out.println("relationship created for send message 'not sent'");
        Thread.sleep(3000);
        act.dragAndDrop(wd.findElement(roundEmailSent), wd.findElement(triangleExit)).build().perform();
        System.out.println("relationship created for send email 'sent'");
        Thread.sleep(3000);
        act.dragAndDrop(wd.findElement(roundEmailNotSent), wd.findElement(triangleExit)).build().perform();
        System.out.println("relationship created for send email 'not sent'");
        Thread.sleep(2000);
    }

    public void reliableClick(By locator) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(wd, 15);
        WebElement element=null;
        Thread.sleep(1000);
        int tries = 0;
        while(tries<3) {
            try {
                element = wait.until(ExpectedConditions.elementToBeClickable(locator));
                break;
            } catch (StaleElementReferenceException ignored) {
                tries++;
                Thread.sleep(2000);
            }
            catch (WebDriverException ignored) {
                tries++;
                Thread.sleep(2000);
            }
        }

        element.click();
    }

}
