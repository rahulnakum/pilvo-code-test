package com.pilvo.ui.base;

import com.pilvo.ui.pages.CreateAppPage;
import com.pilvo.ui.pages.HomePage;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class BaseTest {

    protected Properties properties;
    private String PROP_FILE = "project.properties";
    protected HomePage homepage;
    protected CreateAppPage createApppage;
    protected WebDriver driver;

    public BaseTest() {
        System.out.println("####################################################################");
        System.out.println("#####          TESTS FOR CREATE APP UI                          ####");
        System.out.println("####################################################################");

        //read properties
        properties = loadProperties(PROP_FILE);
    }


    public static Properties loadProperties(String fileName) {
        try {
            String filePath =
                    System.getProperty("user.dir")
                            + File.separator + "src" + File.separator + "test" + File.separator + "resources"
                            + File.separator + fileName;

            Properties properties = new Properties();
            InputStream input = new FileInputStream(new File(filePath));
            properties.load(input);

            System.out.println("Loaded properties - "+ fileName);
            System.out.println("properties : "+properties);

            return properties;
        } catch (IOException e) {
            System.out.println("Failed to find properties file");
            throw new RuntimeException(e);
        }
    }


}
